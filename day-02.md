## Laravel 專案結構
由於我們只以 Laravel 作為 API server，所以只會介紹開發 api 會用到的資料夾以及相關檔案。

* composer.json
* .env
* app
* config
* database
* routes
* storage


### composer.json
還記得昨天我們有安裝 "composer" 作為 php 套件管理工具嗎? 像是 laravel 初始工具，我們在執行指令的時候，有使用到全局的選項 <code>--global</code>，然而每個專案的需求都不同，不適合將所有套件都裝在全局中。因此個別專案的套件就會記錄在個專案底下的 composer.json 當中。

### .env
在 Laravel 中，專案全局的設定，都會記錄在 .env 中，像是資料庫或是其他服務的連線設定。他相對於 .Net MVC 專案中的 web.config。

由於每個開法者的環境不太一樣，所以 .env 是不會被傳到 git 上的。取而代之的是 .env.example。
因此假若在開發過程中，有加入自己的參數，切記要在 .env.example 中也同步加上範例和說明。

專案初始的時候，.env 的注意事項:
1. 如果沒有 .env 請複製 .env.example 並改名為 .env
2. 開啟 .env，若 "APP_KEY" 沒有值，輸入下列 laravel 指令產生
``` shell script
php artisan key:generate
``` 
3. 有用到各種其他服務，像是資料庫、AWS 記得要填上相對資訊

### app
app 資料夾基本上放置了所有 PHP code。像是 MVC 的 controller 位在 app/Http/Controllers，而對應資料庫的 Model 預設會產生在 app 底下 (如 User.php) ，如果覺得檔案太雜想整理到資料夾也行，記得將 model 中的 namespace 作相對應的調整。

app 底下的其他資料夾，會在後續介紹各項功能時候更進一步介紹。

### config
毫無疑問底下會是各種設定檔，與 .env 不同的地方在於 config 底下的檔案，主要用於程式以及系統運作的資料設定。例如，我們打開 `database.php`，我們可以看到資料庫各項設定，同時我們也可以看到，在程式裡面若需要使用到 .env 中的設定，可以使用 `env()` 方法取得([env詳細說明](https://laravel.com/docs/5.8/upgrade#environment-variable-parsing))。

這次鐵人主要有用到的幾個的設定為 `app.php`、`auth.php` 以及另外兩個由套件產生的設定檔。

### database
系統之後要使用的 table schema 都會產生在 `database\migrations` 底下。在預設的專案裡面，已經存在下面兩張 schema:
* create_users_table.php
* create_password_resets_table.php

之後將透過這些 schema 設定檔:
* 對 table schema 版控
* 建立 Model
* 對應 Resource

### routes
如果有寫過其他 MVC 專案，Laravel 比較不同的是，網站中的每一個 URL 都可以自行定義要對應到哪個 controller 底下的 function，由於我們只將 Laravel 作為 API server，因此我們只會用到 `api.php`。

### storage
當前端上傳檔案，同時我們預設檔案室存於本機時，透過 Laravel 的方法 `storage()` 可以對檔案存取於 `storage/app` 底下。



意外廢話有點多，先喘口氣，明天開始來建立資料庫!