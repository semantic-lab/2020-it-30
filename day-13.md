今天主要說明為甚麼 Postman 明明發出 request 有正確的 response，可是為甚麼接下來不論是 Vue.js 或 Nuxt.js 都會失敗呢!

[同源準則(Same-origin policy)](https://developer.mozilla.org/zh-TW/docs/Web/Security/Same-origin_policy)，簡單來說，A 網站不能「任意」存取 B 網站的資源，觀念有點像是「只有自己可以打自己家的小孩」。如此一來 ITHome 才不會隨便被惡意網站存取、修改資料。

要符合同源準則要有三個項目相同，包括「protocol」、「domain」和「port」三個部分，只要其中一個不一樣就算不同源。
* protocol：HTTP、HTTPS、FTP ... 等
* domain：包含 1、2、3 級域名，例如 `https://ithelp.ithome.com.tw/users/` 中的 `ithelp.ithome.com.tw`
* port：80、8080、3000 ... 等

所以根據上面同源的定義，假若我們在同一部機器跑兩個前後端網站，理論上一定不同源，所以由 JavaScript 發出去的 `XMLHttpRequest` 就會得到 `No Access-Control-Allow-Origin` 的回應。這時候其實只要在 server 端加上 [跨來源資源共用（CORS）](https://developer.mozilla.org/zh-TW/docs/Web/HTTP/CORS)即可。

Laravel 處理 CORS 的方法如下:
01. 輸入下面指令安裝套件
```shell
composer require barryvdh/laravel-cors
```

02. 打開 `app\Http\Kernel.php` 在 `$middleware` 中加入以下設定
```php
protected $middleware = [
    // ...
    \Barryvdh\Cors\HandleCors::class,
];
```

03. 再輸入下面指令產出設定檔
```shell
php artisan vendor:publish --provider="Barryvdh\Cors\ServiceProvider"
```

04. 打開 `config\cors.php` 客製設定
* supportsCredentials：
* allowedOrigins：預設為 `*` 代表允許所有來源網站，一般來說只會設定白名單，例如前端對接的網站 `http://localhost:3000`
* allowedHeaders：預設為 `*` 代表允許所有標頭 (即請求類型)
* allowedMethods：預設為 `*` 代表允許所有 http request methods

---
今天是基礎 Laravel 的最後一篇介紹。除了 DI 的部分或許有點抽象，其他的部分是都還滿平易近人的，不過這當然也是因為小弟是以可以建立 API 為前提的基礎介紹。歷年來有許多大大早有有詳細的框架設計、運作解析，大家有興趣的話可以在更深入了解!

明天喘口氣 (我是說我 XD)，簡單介紹 GIT sub-module 功能性，如何讓之前提到的 request validation 規則與前端開發共享，接著就要進入 Nuxt.js 的部分了!
