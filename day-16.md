Vue 基礎的概念就是將畫面切割成一個個元件 (component)，在各種需要的地方插入元件提供更方便、可重複利用的前端開發。至於元件的定義，可以大到整個完整的畫面，或是小到一個單純的按鈕。而所有元件的相關定義、設定甚至是運作都會寫在 `*.vue` 的檔案當中。接著我們就以下面主題快速看過 Vue 元件吧!

* [基本架構](#基本架構)
* [Vue 常用屬性](#Vue-常用屬性)

#### 基本架構
.vue 分為三個部分，包括 `template`、`script` 和 `style`，個別執掌的功能：
* `templae`：畫面 HTML 的部份，包括部份 JavaScript 渲染邏輯和資料綁定。值得注意的是，`<template></template>` 底下只能有唯一一個 root dom。

* `script`：整個元件需要用到的 JavaScript。基本上這裡主要是撰寫 生成 `VueComponent` 實例的一些設定檔。

* `style`: 撰寫元件的樣式。如果帶有 `scope` 屬性，則代表即便其他地方有相同的 CSS selector 定義，但這些樣式只會套用在這個原件上。

```html
<template>
    <!-- 只能有一個 root dom -->
    <div class="vue-component-example">
        <!-- ... -->
    </div>
</template>

<script>
    export default {
        // Vue instance 的各種屬性和設定
    }
</script>

<style scope>
    .vue-component-example {
        /* ... */
    }
</style>
```

#### Vue 常用屬性
下面將列舉常用 Vue 屬性，並個別介紹功用。

```javascript
export default {
  name: "personalInfo",
  mixins: [],
  components: {},
  props: {},
  data() {
    return {};
  },
  computed: {},
  watch: {},
  methods: {},
  mounted() {},
}
```

* `name`：單純元件的名稱。

* `mixins`：加入其他 Vue 設定檔並套用功能。概念「像」是多重繼承 (聽說 vue 3.X 要捨棄掉了)。常見的運用在於，畫面可能完全不一樣或是完全交由使用者客製，但各種資料、方法和計算邏輯，是固定的。

* `components`：各種其他元件。要使用的其他元件必須是註冊於全局或者個別加在此屬性底下。

* `props`：由外面帶進來的資料(參數)。Vue 元件具有高供彈性，像是 function 可以帶入參數。

* `data`：本身內部定義的資料變數)。這裡比較特別的是定義的變數必須包含在回傳的物件當中，例如我們定義了 `userName` 則應該撰寫如下：
    ```javascript
    export default {
        data() {
            return {
                userName: undefined,
                // ...
            };
        }
    }
    ```

* `computed`: computed 很特別，他底下的都是具有回傳值的 function，主要是希望透過特定的邏輯或計算產出動態變動的值，例如，假設 api 只有提供使用者的 `firstName` 和 `lastName`，若要顯示完整名字，就可以透過 `computed` 產生一個包含晚整名稱的新變數

* `watch`：用來監控 `props`, `data` 和 `computed` 三項 VueComponent 實例的變數。

* `methods`：掛在 VueComponent 實例底下的各種方法。

* `mounted`：是一個 VueComponent 實例初始化的時候會執行的方法。
---
這裡有幾個部份需要注意：
01. `this` 是 VueComponent 實例，其中 `props`, `data` 和 `computed` 三者的變數經過初始化之後都會是 `this` 的屬性，所以命名的時候要注意不要重複。
    ```javascript
    export default {
        props: {
            showType: {
                type: String,
                default: 'grid'
            }
        },
        data() {
            return {
                firstName: 'Albert',
                lastName: 'Lin',
                email: 'xxxxx@gamil.com',
                phone: '09000000000'
            };
        },
        computed: {
            fullName() {
                return `${this.firstName} - ${this.lastName}`;
            }
        }
    }
    ```
    【圖】

02. `computed` 是以屬性方法使用，而不是呼叫 function，例如上面範例當中要得到完整性名應該使用 `this.fullName` 而不是 `this.fullName()`

03. `mounted` 的執行時機會是在 `<template>` 裡面的資料渲染完成之後，例如下面的程式碼，在 console 中會先看到「template rendered」再看到 「mounted invoked」：
    ```html
    <template>
        <div v-html="'<script> console.log(\'template render\'); </script>'"></div>
    </template>

    <script>
        export default {
            data() {
                return {};
            },
            mounted() {
                console.log('mounted invoked');
            }
        }
    </script>
    ```
【圖】

---
今天主要介紹 `*.vue` 檔案的基本架構，以及常用的屬性，明天會介紹 `<template>` 渲染、資料傳遞和綁定的部份!
