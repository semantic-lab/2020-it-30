# RRR撞到不負責之 Laravel + Nuxt.js 踩坑全紀錄

1. ENV
    * WHY MVVM
    * backend
        * wamp
        * composer
        * Laravel
    * frontend
        * npm
        * nuxt
        * git
2. Laravel
	* 建立
	* 專案結構
	* httpd 設定
3. Laravel Migration - 一切都從資料庫說起
	* env
	* 指令 
	* code-first
4. Laravel Model & Resource
5. Laravel Injection 依賴注入
6. Laravel Repository
7. Laravel Service
8. Laravel Controller 1
9. Laravel Controller 2
	* FormRequest
	* Validation
	* 套件
10. GIT sub-module MVVM 的好幫手
11. Laravel API route & 實測 (Postman)
12. Vue 與 Nuxt
	* Nuxt 建立 & 路徑包含空白
	* 專案結構
	* npm run dev
13. Vue component
	* the config
	* v-model
	* v-if
	* v-for
	* event (v-on) & emit
	* v-bind
14. Vue v-for
15. Vue Route
	* route & router
	* query
	* param
16. Nuxt Page & Route
	* asyncData
	* fetch
	* context
17. Nuxt Layout
18. Nuxt api 串接
	* validation
	* 套件
	* Laravel Middleware for CROS 同源
	* async/await
	* Promise.all
19. Nuxt Plugin
20. Nuxt Vuex & Cookie
21. Nuxt no-ssr
	* window & document undefined
22. Laravel 部屬 docker
23. Nuxt 部屬 docker
24. Style SCSS
	* @import
	* @mixin
	* @include
	* theme.scss
25. Form: view, create & edit
26. Table: Table, Search & Pagenation
27. 
28.
29.
30.
31. 心得











