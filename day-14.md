在之前有提到前後端共享驗證規則進行開發，這樣的概念用 `Git Submodule` 就可以進行。Git submodule 的概念是，將其他 repositories 引入專案作為一部份。例如我們有時候會使用開源的一些 libraries，如果這些 libraries 並沒有上到各大套件管理平台如: Packagist、NPM、NuGet ... 等，那麼使用上通常我們會先 clone 一份回來，或者直接下載並加入道專案當中。

然而這樣最常出現的問題是，假若套件進行了修改，我們一定無法在第一時間知道 (除非天天關注)，但若使用 submodule 方式就可以知道對方的異動，例如 [`LaraDock`](https://laradock.io/) 就是提供以此方式進行。同樣的概念可以套用在前後端兩個不同專案的共享文件或是程式碼的部分。建立 submodule 的方式頗為簡單，作法如下:

01. 在各大 Git 平台上創建一個新的 repository (後續稱為 submodule-repo)

02. 根據需求為 submodule-repo 建立新的 branch (後續稱為 request-branch)。這個部分未必需要，例如說前後端要共享的 code 或是文件可能在現有專案分屬不同資料夾，那就有可能需要由多個 branches 管控。

03. 回到原專案，執行下列指令將 submodule-repo 的 request-branch 加為專案的一部份。
```git
git -c diff.mnemonicprefix=false -c core.quotepath=false submodule add -b <branch-name> -f <repository-url> <local-folder>
```
要注意的是如果 submodule-repo 的內容要加在特定的資料夾底下，可以在最後 `local-folder` 的部分填入資料夾的路徑，同時這個資料夾必須是空的。例如說我們希望將所有 `config/validators` 底下的 JSON 檔案由 submodule-repo 控管，因此指令就會是：
```shell
git -c diff.mnemonicprefix=false -c core.quotepath=false submodule add -b request-branch -f https://gitlab.com/XXXX/submodule-repo.git config/validators
``` 

04. 未來當屬於 submodule 的檔案有異動，就必須回到 submodule-repo (request-branch) 中進行 commit 和 push，原始專案的 git 只會有紀錄 submodule-repo 的異動編號 (如圖)。

---
在開始 Nuxt.js 之前，簡單的介紹好用的 submodule，實現之前提到的 request validation 規則可以同步提供前端開發。明天我們就進入尾聲看看 Nuxt.js 吧!