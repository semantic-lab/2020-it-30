距離[Day 02](https://ithelp.ithome.com.tw/articles/10213452) 也幾天了，在介紹完基本 Laravel API 開發之後，今天開始要介紹 **Nuxt.js**。和多數的介紹不同，小弟比較喜歡透過造輪子了解特性 ~~(其實只是不學無術 XD)~~ ，所以今天開箱完專案結構之後，並不會特別去說明 Vue 或是 Nuxt 的生命週期。

* [設定檔](#設定檔)
    * package.json
    * nuxt.config.js
* [畫面](#畫面)
    * [layouts](#assets)
    * [pages](#assets)
    * [components]](#assets)
* [資源](#資源)
    * [assets](#assets)
    * [static](#assets)
* [狀態](#狀態)
    * [store](#assets)
* [其他](#其他)
    * [middleware](#assets)
    * [plugins](#assets)


#### 設定檔
* `package.json`：專案 npm 套件管理檔案。
* `nuxt.config.js`：專案運行相關的設定，包括 head (標頭)、全局 css、plugin (nuxt 套件)以及 axios 的設定等。

#### 畫面
Nuxt 有明確的將畫面分成不同階層：
* `layouts`：固定的畫面樣板，屬於最外層，透過設定於 `pages` 底下的 Vue component (後續稱為 *.vue) 中使用。
* `pages`：網站的每一個畫面，`pages` 底下的目錄與檔案結構會直接影響最終畫面對應的 URL (即取代了 vue-router 的設定)，另外因為要處理 SSR 預先仔入資源問題，因此 `pages` 底下的 *.vue 中會有些特殊的屬性 (只有在 `pages` 底下才有作用)，這部分之後會詳細介紹。
* `components`：是將畫面中部份可共用的區塊獨立出來、重複使用的元件，`layouts\*.vue` 和 `pages\*.vue` 都可以使用

#### 資源
* `assets`：這裡面的資源像是 JavaScript、CSS、SCSS 等可能需要經過 `Webpack` 轉譯的檔案。
* `static`：靜態資源和 `assets` 相反，主要是放置不需要轉譯、可以直接使用的檔案，例如圖檔之類的。

#### 狀態
* `store`：和 Vue 一樣，是存放各種網站全局狀態(變數)的地方。這裡面只有 `index.js` 會執行特定的方法，之後會進一步介紹。

#### 其他
* `middleware`：如同 Laravel 一樣，這裡是撰寫各種進入畫面前的過濾邏輯，例如登入者本身是一般使用者，則不可拜訪需要管理員權限的頁面。
* `plugin`：無論是自己撰寫或是別人的套件，如果在 `layouts`、`pages` 或是 `components` 中頻繁的引入、使用，可以改寫為 plugin，成為 vue instance 或是 nuxt context 的其中一部份，由框架自動引入，而不需要每個 `*.vue` 都一一引入。
 
 ---
 基本上原廠標配的 Nuxt 專案架構，跟 Vue Cli 大同小異，主要的差別將會在 `vue-router` 以及 `pages\*.vue` 的設定跟運行有比較大的不同，尤其是 `pages\*.vue`，因為要達到 SSR 任務，所以在 life cycle 方面會有些微的調整。

 ---
 明天我們會簡單的帶過 Vue component 的基礎屬性和用法，小弟曾經踩到幾個比較大的雷會另外獨立加以說明。接下來所有介紹會以 **`ES6`** 語法撰寫 (有語法糖真的比較好寫)，如果有不熟的部分可以參閱「[ECMAScript 6 入门](https://es6.ruanyifeng.com/)」!