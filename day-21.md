在 Vue 專案中，透過設定 `VueRouter` 制訂各個頁面的 URL 以及各種畫面結構安排，但在 Nuxt 專案中，不再需要透過設定，`pages` 的目錄結構會自動對應出 `VueRouter` 的設定。

* [基礎路由](#基礎路由)
* [動態路由](#動態路由)
* [嵌套路由](#嵌套路由)
* [客製設定](#客製設定)

#### 基礎路由
透過 `pages` 底下的資料夾與檔案路徑，Nuxt 會自動生成 `VueRouter` 實例，不再需要自行定義。

【圖 basic route】

在上圖的目錄結構中會產生下列路由與 page component 之間的對應：
路由 (URL) | page component | 路由名稱 (Route Name)
:---|:----------------|:----------
/manager | pages/manager/index.vue | manager
/manager/user | pages/manager/user/index.vue | manager-user
/manager/user/create | pages/manager/user/create.vue | manager-user-create
/manager/user/edit | pages/manager/user/edit.vue | manager-user-edit
/manager/user/view | pages/manager/user/view.vue | manager-user-view

自動生成的 VueRouter 設定如下：
```javascript
new VueRoute([
  {
    name: 'manager',
    path: '/manager',
    component: '~/pages/manager/index.vue',        
    chunkName: 'pages/manager/index'
  },
  {
    name: 'manager-user',
    path: '/manager/user',
    component: '~/pages/manager/user/index.vue',
    chunkName: 'pages/manager/user/index'
  },
  {
    name: 'manager-user-create',
    path: '/manager/user/create',
    component: '~/page/manager/user/create.vue',
    chunkName: 'pages/manager/user/create'
  },
  {
    name: 'manager-user-edit',
    path: '/manager/user/edit',
    component: '~/pages/manager/user/edit.vue', 
    chunkName: 'pages/manager/user/edit'
  },
  {
    name: 'manager-user-view',
    path: '/manager/user/view',
    component: '~/pages/manager/user/view.vue', 
    chunkName: 'pages/manager/user/view'
  },
  // ...
]);
```

基礎路由規則為：
01. 路由為 `<檔案路徑>/<page component 檔案名稱>`，其中檔案路徑不包含「pages」。
02. page component 檔案名稱如果是 `index`，則路由為 `<檔案路徑>`。
03. 路由名稱規則與路由相似，只是由「-」符號連結。

#### 動態路由
如同 Laravel 可以在路由中帶入參數，對此稱為動態路由 (Dynamic Route)。由於 Nuxt 沒有 VueRouter 可以設定，因此路由中的變數，就會成為資料夾或是 page component 的名稱。

假設我們希望在 「編輯特定使用者」和 「瀏覽特定使用者」兩個頁面的路由帶上變數 `userId`，則會需要建立名稱為 `_<變數名稱>` 的資料夾：

路由 (URL) | page component | 路由名稱 (Route Name)
:---|:----------------|:----------
/manager/user/`userId`/edit | pages/manager/user/`_userId`/edit.vue | manager-user-`userId`-edit
/manager/user/`userId`/view | pages/manager/user/`_userId`/view.vue | manager-user-`userId`-view

如果變數是帶在路由的最後面如 `/manager/user/view/userId` ，則 page component 命名為 `_<變數名稱>.vue`：

路由 (URL) | page component | 路由名稱 (Route Name)
:---|:----------------|:----------
/manager/user/view/`userId` | pages/manager/user/view/`_userId.vue` | manager-user-view-`userId`

#### 嵌套路由
在畫面的設計上，很常會有同一個系列的畫面有一部份是相同的，例如 `pages/manager/user/` 底下的 page components 都有相同的側邊選單。在 Nuxt 中可以設計一個用來嵌套的子樣板，這樣開發與維護就變得精簡。

嵌套路由本身也是一個 page component，其命名規則為：
01. 嵌套樣版名稱與要嵌套的檔案資料夾名稱相同
02. 嵌套樣版與要嵌套的檔案資料夾在同一層

例如下圖，`pages/manager/user.vue` 會嵌套到 `pages/manager/user` 底下所有 page components。

【圖】

在嵌套樣版中利用 `<nuxt-child>` component 來置換不同 page componsnt 內容：
```html
<template>
    <div>
        <div>來自嵌套樣版 user.vue 的文字</div>
        <div>
            <nuxt-child></nuxt-child>
        </div>
    </div>
</template>
```
對應的 VueRouter 如下：
```javascript
new VueRouter([
  {
    name: "manager",
    path: "/manager",
    component: "~/pages/manager/index.vue",
    chunkName: "pages/manager/index"
  },
  {
    path: "/manager/user",
    component: "~/pages/manager/user.vue",
    chunkName: "pages/manager/user",
    children: [
      {
        name: "manager-user",
        path: "",
        component: "~/pages/manager/user/index.vue",
        chunkName: "pages/manager/user/index"
      },
      {
        name: "manager-user-create",
        path: "create",
        component: "~/pages/manager/user/create.vue",
        chunkName: "pages/manager/user/create"
      },
      {
        name: "manager-user-edit",
        path: "edit",
        component: "~/pages/manager/user/edit.vue",
        chunkName: "pages/manager/user/edit"
      },
      {
        name: "manager-user-view",
        path: "view",
        component: "pages/manager/user/view.vue",
        "chunkName": "pages/manager/user/view"
      }
    ]
  }
]);
```

#### 客製設定
雖然 Nuxt 會自己透過 `pages` 目錄結構繫結 `VueRouter` 設定，但 Nuxt 仍然在 `nuxt.config.js` 的 `router` 屬性，提供我們做額外的設定：

* `base`：是用來指定整個應用的基底 URL，例如下面的例子，在執行 `npm run dev` 之後，專案起始 URL 會是 `localhost:3000/app`。
```javascript
export default {
  // ...
  router: {
    base: '/app/'
  }
  // ...
}
```

* `routeNameSplitter`： 在先前介紹路由名稱的時候有提到適用「-」符號連結，在這個屬性我們可以自行設定連結的符號。下面的設定，`pages/manager/user/edit.vue` 的路由名稱會是 `manager => user => edit`。
```javascript
export default {
  // ...
  router: {
    routeNameSplitter: ' => '
  }
  // ...
}
```

* `extendRoutes`：在 VueRouter 石礫中增加額外自訂的路由，例如下面是一個轉址路由的範例。
```javascript
export default {
  // ...
  router: {
    extendRoutes (routes, resolve) {
      routes.push({
        path: '/manager/user/create', redirect: '/manager/user/0/create'
      });
    }
  }
  // ...
}
```
---
如果是從 Vue 專案轉到 Nuxt，在路由的設定可能會稍有不習慣，還好 Nuxt 還是有保留客制設定的部分，我們一樣可以設定出各種不同需求的路由。在看完 `page` 的路由規則與相關設定之後，明天要跟大家介紹 Vuex 與 Cookie!

