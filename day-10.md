在介紹完 controller 之後，接著要為 api 綁定 URL 讓其他系統可以使用。今天會依照下面主題逐一介紹 Laravel Routing 設定。
* [基本用法](#基本用法)
* [動態參數](#動態參數)
* [route 分組](#route_分組)
* [其他設定](#其他設定)


## 基本用法
所有的 api 對應的網址都會寫在 `routes` 底下的檔案中，我們可以依照需求分在不同檔案，但目前我們只依照 Laravel 基本的分類寫在 `routes\api.php`。


所有的方法都是呼叫 `\Illuminate\Support\Facades\Route` (以下簡稱 Route) 而來，如果 IDE 連結不到原始的類別，可以安裝 [barryvdh/laravel-ide-helper](https://github.com/barryvdh/laravel-ide-helper)，讓開發更簡便。

五個常用的 HTTP 方法 (`GET`、`POST`、`PUT`、`PATCH`和`DELETE`)，會是最終、也是最簡單綁定 URL 和 API 方法的功能。這五個方法，第一個參數是自訂義的 URL；第二個參數勢閉包或是對應的 controller function (我們稱為) action。

```php
Route::get('v1/post/view', 'PostController@read');

Route::post('v1/post/create', 'PostController@create');

Route::put('v1/post/update', 'PostController@edit');

Route::patch('v1/post/update/title', 'PostController@editTitle');

Route::delete('v1/post/delete', function (Request $request) {
    // ...
});
```

從上面的範例可以看到 `get()`、`post()`、`put()`和`patch()`，我們都是以設定對應 action 方式綁定；`delete()` 則是使用閉包的方法。如果有一些沒有太多商業邏輯或是極其簡單的資料，或許可以直接在閉包裡面處理和回傳 response。不過小弟個人主要還是以設定 action 方法處理。

## 動態參數
我們有時因應需求，會需要在 URL 中間帶上資料，但又不想使用 query string，此時
只要在 URL 中間以 `{<變數名稱>}` 設定即可。 
```php
// api.php:
Route::post('v1/post/{postId}/update', 'PostController@edit');

// PostController.php:
class PostController extends Controller
{
    public function edit($postId, FormRequest $request)
    {
        // ...
```
例如上面的例子，當我們呼叫 `v1/post/2764/update` 時，首先會根據 `api.php` 的設定執行 PostController 的 edit 方法，同時會將「2764」帶入 `$postId` 中。只要變數名稱與 route 中設定的一樣，不論參數位置的順序，都會正確的帶入。

在一些狀況下，我們要讓參數具有預設值，如同上面的設定，並在變數名稱後面加上「?」 如: `{<變數名稱>?}` 即可。
```php
// api.php:
Route::post('v1/post/eidt/{title}', 'PostController@editTitle');

// PostController.php:
class PostController extends Controller
{
    public function editTitle($title = "no title", FormRequest $request)
    {
        // ...
```

在動態變數中 Laravel 提供方便驗證變數是否合法的方式 `where()`，
期第一個參數是變數名稱，第二個參數是驗證變數值得正則表示式；如果一個 URL 有多個變數需要驗證可以改用 array 的方式。
```php
Route::post('v1/post/{postId}/update', 'PostController@edit')->where('postId', '[0-9]+');
```

## route 分組
從一開始五個常用的 HTTP 方法設定範例中我們可以看到每一個 URL 都有共同的部份 `v1/post`，在 Laravel 中透過分組 (groups) 的寫法，進一步整合管理，減少重複撰寫相同的部份或是共同的設定。以下介紹幾個常用的分組方法，完整介紹請參考[官網](https://laravel.com/docs/5.8/routing#route-groups)。

* `prefix`: 前缀是用來設定 URL 開始共同的部份，如上面 HTTP 方法的範例，我們可以改寫為:
```php
Route::prefix("v1/post")->group(function () {
    Route::get('view', 'PostController@read');
    Route::post('create', 'PostController@create');
    Route::put('update', 'PostController@edit');
    // ...
});
```

* `namespace`: 假若我們要綁定的 controller 不是在預設的 `app/Http/Controller` 裡面，而是有更進一步的分類，這時候可以設定 `namespace()` 方便管理。例如我們將 controller 分在 `app/Http/Controller/Admin` 和 `app/Http/Controller/User` 兩個資料夾，我們就可以設計如:
```php
Route::namespace('Admin')->group(function () {
    Route::get('v1/admin/manager/create', 'UserController@createManager');
    Route::get('v1/admin/user/{id}/delete', 'UserController@delete');
    // ...
});
```

* `middleware`: Middleware 是 Laravel 在進入 action 之前會先對 http request 進行過濾、檢查的地方，這部份我們會在明後天更進一步的說明。在 route 中，我們可以將有共同過濾或檢查機制管理在一起。
```php
Route::middleware('adminonly')->group(function () {
    Route::get('v1/admin/manager/create', 'UserController@createManager');
    Route::get('v1/admin/user/{id}/delete', 'UserController@delete');
    // ...
});
```

## 其他設定
所有在 `api.php` 設定的 routing，完整網址都是由 `<domain 或是 IP>/api/....` 組成。我們可以打開 `app\Providers\RouteServiceProvider.php`，在 `mapApiRoutes()` 中客製化自己的設定，
```php
    protected function mapApiRoutes()
    {
        Route::prefix('api') // 可以改成自己的 prefix 或是移除，以下類推
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php')); // 這裡定義了要套用到哪一個設定檔
    }
```

另外，我們在綁定了 URL 與 action 之後，我們可以使用[postman](https://www.getpostman.com/downloads/)這個很好用的發送 http request 工具，測試我們所寫的 API。

下圖中，
01. 我們會先輸入 API 網址，(要記得將 wamp 或是其他 apache 套裝包啟動)
02. 如果要在 request 中帶資料，通常我們會使用 JSON 格式資料
03. 輸入 request body 資料
04. 送出之後，我們會在下方看到 API 回傳的 response 資料


---
終於! 到目前為止我們可以用 Laravel 寫出完整的 API 服務，學習曲線個人認為滿舒服的，大家可以試試看!  明天我們將進一步地叫紹 Laravel Middelware 的部分，像上面說的，透過 middlewares 可以讓我們進到 action 之前就先檢查、過濾不合法的 request，例如，一般使用者的身分不得執行或是呼叫管理者的 api 等諸如此類的保護機制!