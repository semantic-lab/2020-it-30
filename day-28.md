太久沒有用 Docker 差點忘光了，今天主要介紹基本的 Docker 指令以及如何使用 `laradock` 架設 Laravel 專案，沒錯，你以為惱人的環境設定已經結束了嗎? 錯! 麻煩的現在才要開始 QAQ

* [Docker](#docker)
* [docker 檔案與指令](#docker-檔案與指令)
* [laradock](#laradock)

#### Docker
這裡是假設大家對 Docker 有一點概念，但沒實際操作過為主，如果還不知道 [Docker](https://philipzheng.gitbooks.io/docker_practice/content/introduction/what.html) 是甚麼的可以先去看看喔!

如果各位跟小弟一樣沒有 Windows 10 Pro/Enterprise 高級環境的話， ~~可以來互相取暖 (誤 XD)~~，會是使用 docker toolbox ，他本身還是用 VM，詳細安裝細節可以參考[官方文件](https://docs.docker.com/toolbox/toolbox_install_windows/)就不贅述!

當初在使用 docker 時候跟 VM 有關的問題包括：

01. 系統配置：建議改為 2 CPU / 4G
02. 共享資料夾：加入專案 (Laravel & Nuxt) 的路徑

當然你會發現 VM 根本不給改啊! 別擔心照著[這份文件](https://github.com/crops/docker-win-mac-docs/wiki/Windows-Instructions-(Docker-Toolbox))改吧!

#### docker 檔案與指令
Docker 真的是博大精深，這裡是快速簡述常用的檔案與指令，執行指令要用剛安裝起來的 `Docker Quickstart Terminal`。這裡稍作提醒，雖然不是所有 docker 指令都需要在當前專案目錄下執行，但建議每次要操作的時候還是切到專案目錄下，不然有時候操作到一半因為不在正確目錄下出現報錯訊息，看了就很阿雜 XD

* `.env`：是用來撰寫整個 docker 服務的一些全域變數的檔案跟 Laravel 的 .env 有一樣的概念。

* `Dockerfile`： 是用來撰寫建立一份 image (映像檔) 所有需要準備的工作以及各種設定，例如要安裝 Apache 要在系統內安裝那些東西、設定些甚麼，都在此檔案中處理，與 Dockerfile 和 image 有關的指令如下：

    指令 | 說明
    :----|:-----
    `docker images` | 顯示當前機器內所有存在的 images。
    `docker build -t <IMAGE NAME>:<TAG NAME> .`| 利用 Dockerfile 建立 image 並且加上 tag 區分與識別，最後的「`.`」 不可以忽略啊!
    `docker rmi <IMAGE ID>` | 刪除當前機器內特定的 image。
    `docker rmi -f <IMAGE ID>` | 強制刪除當前機器內特定的 image。
    `docker rmi -f $(docker images -aq)` | 強制刪除當前機器內所有的 images。

* `docker-compose.yml`：撰寫要用到的所有服務要用到的資訊，以及各種服務之間連結設定的檔案，例如要架設 Laravel 站台需要 Apache、MySQL、phpmyadmin 這些服務，那這些服務如何對接、以及各項設定值都是在這裡設定，與 docker-compose 有關的指令如下：

    指令 | 說明
    :----|:-----
    `docker ps` | 顯示當前機器內所有運行的 container (容器)。
    `docker-compose ps` | 顯示同時存在於「當前機器」以及 「`docker-compose.yml`」 的 container (容器)。
    `docker-compose up <SERVICE NAME 0> <SERVICE NAME 1> -d` | 啟動 `docker-compose.yml` 有列出來的各項服務，如果這些服務所需要的 images 還沒存在於當前機器的 docker 內，docker 會自己建立或下載一份。最後參數 `-d` 是指要不要在背景運行的選項，小弟個人在測試的過程中通常為了要看一些 log 所以通常不加。
    `docker-compose build <SERVICE NAME 0>` | 對於當前存在的服務重新建構。
    `docker-compose exec <SERVICE NAME> sh` | 有時候需要在 container 的 terminal 下指令，例如 `php artisan`、`composer i` 或是 `npm i` 的時候，就可以藉由這個指令進到 container 裡面。這裡要注意要用 service name 而不是 container name! 另外要退出 container 並回到 docker 要下 `exit`；最後的 `sh` 通常在網路上的教學或文件都是用 `bash`，但個人在 toolbox 的環境只能使用 `sh`，提供大家參考。
    `docker rm $(docker ps -aq) -f` | 強制刪除當前機器內所有的 containers。

#### laradock
有了大致的概念之後，我們要來架設 Laravel 啦! 這裡推薦使用 [`laradock`](https://laradock.io/getting-started/#2-2-installation)。

最簡單的方法是將 laradock clone 一分到專案裡面，如果想要不斷追蹤 laradock 的最新開發，可以使用 Git submodule，今天主要使用 clone 的方式進行。

01. 在 cmd 中執行下列指令 clone 到專案裡：
    ```shell
    git clone https://github.com/laradock/laradock.git
    ```

02. 進入到 `laradock` 裡面，複製 env-example 為 .env (印象中 clone 下來 .env 就有)。

03. nginx 調整 (我們會用輕量的 nginx 取代 Apache)：
    * `.env` 中的 `NGINX_HOST_HTTP_PORT` 改為 **8080** 或是其他 port 因為，80 之後要提供給 Nuxt 使用的。

04. MySQL 調整：
    * `.env` 中的 `MYSQL_USER`和`MYSQL_PASSWORD` 要跟 laravel .env 設定成一樣。
    * `laradock/mysql/my.cnf` 中的 `sql-mode` 要在最後加上 `,NO_AUTO_CREATE_USER`。
    * `laravel .env` 中的 `DB_HOST` 改為 `mysql`，因為在 nginx container 裡面不會有 MySQL，所以要連到 mysql container。
    * 如果出現下列錯誤，要在 `docker-composer.yml` 中的 `mysql` 加入 `command: "--innodb_use_native_aio=0"`。
        ```shell
        [ERROR] InnoDB: Operating system error number 22 in a file operation
        ```
        或
        ```shell
        php_network_getaddresses: getaddrinfo failed: Name or service not known
        ```
    * 如果出現下面錯誤，則到 `C:\Users\<USER_NAME>\.laradock\data` 底下把 `MySQL` 資料夾整個刪除。
        ```shell
        initialize specified but the data directory has files in it. Aborting
        ```
05. phpMyAdmin 調整：
    * `.env` 中的 `PMA_USER` 和 `PMA_PASSWORD` 要設定和 MySql 以及 Laravel .env 一樣。
    * `.env` 中的 `PMA_PORT` 改為 `8081` 或是其他 port，8080 上面有提到要提供 nginx 使用。

06. 最後，執行下面指令開始安裝 images 以及建立 containers (第一次會有點久)
    ```shell
    docker-compose up nginx mysql phpmyadmin
    ```
    如果沒有加「-d」、沒有在背景執行，看看有沒有錯誤訊息。

07. 假設都完成之後，先看看大大的 docker toolbox 分配到的 IP (顯示在 Docker Quickstart Terminal 最上面如： 192.168.99.100)，然後在瀏覽器上鍵入 `IP:8080` 看看 laravel 有沒有正常架起來。

08. 上面沒問題之後進到 `IP:8081` phpmyadmin 中建立資料庫。

09. 接著執行下面指令進入到 laravel 專案的 workspace，執行 `migrate` 看看有沒有成功連上 DB 以及建立成功。
    ```shell
    docker-compose exec worksapce sh
    ```
---
以上就是今天的 docker 基本指令介紹，以及 `laradock` 的使用。喔! 順帶一提，toolbox 查網路的資料說已經停止開發了，建議大家將 Windows 升級 ...，因為使用起來滿多坑效能也沒很好，所以如果剛好要換電腦的捧油，建議大家換到 Windows Pro/Enterprise!



