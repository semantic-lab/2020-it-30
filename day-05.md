開始之前，如果有對 MVC 不熟的大大可以[參考文章](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller)。在 MVC 當中，其實只有定義了 model、view 和 controller 而已，意思就是說不需要 repository 與 service 依然可以執行各種商業邏輯。

我們可以想像，把所有商業邏輯都寫在 controller 是不是有點過於臃腫且重複的程式也不少，未來光是維護的困難性就頗高，更不用說測試的部分。那我們把所有存取資料庫的邏輯都寫到，model 中吧! 然而 model 的定義是用來對應 DB 資料表以及 record 所對應的類別，不適合將商業邏輯寫在這裡面。因此為了將 controller 瘦身，又保持 model 的乾淨，我們在 model 與 controller 中間又增加了 repository 和 service 兩層。

其中 repository 主要負責商業邏輯中各種資料庫 CRUD 的部分，而 service 則是統合各項資源與應用 (service 部分明天再談)。
以下我們會用 Post 作為範例解釋 repository 並進行重構。

```php 
namespace App\Repositories;

use App\Models\Post;

class PostRepository
{
    public function create($data)
    {
        $newPost = new Post();
        $newPost->user_id = $data['user_id'];
        $newPost->title = $data['title'];
        $newPost->content = $data['content'];
        $newPost->published_at = $data['published_at'];
        $saveSuccess = $newPost->save();
        if ($saveSuccess) {
            return $newPost;
        }
        throw new \Exception("create failed");
    }

    public function update($id, $data)
    {
        $post = Post::find($id);
        $post->title = $data['title'];
        $updateSuccess = $post->save();
        if ($updateSuccess) {
            return $post;
        }
        throw new \Exception("update failed");
    }

    public function delete($id)
    {
        return Post::destroy($id);
    }

    public function readById($id, $collumns = ['*'])
    {
        return Post::find($id, $collumns);
    }

    // ...
}
```
在上面的例子，我們建立一個 PostRepository，並且加入簡單的 CRUD functions。進一步，我們可以想像，一些基礎、常用的 CRUD "邏輯"皆相同，
因此不論是 UserReposetory 或是其他 repositories 都差不多，唯一不同的是，每個所依賴的 model 不同。
```php
class UserRepository
{
    public function create($data)
    {
        $newUser = new Post();
        $newUser->name = $data['name'];
        $newUser->email = $data['email'];
        $newUser->password = $data['password'];
        $saveSuccess = $newUser->save();
        if ($saveSuccess) {
            return $newUser;
        }
        throw new \Exception("create failed");
    }

    // ... 
```

因此我們可以考慮重構
01. 建立 interface 定義有哪些共同及常用的資料表存取方法
```php
namespace App\Repositories;

interface IRepository
{
    public function create(array $data);

    public function update($id, array $data);

    public function delete($id);

    public function readById($id);
}
```
02. 建立一個所有 repositories 的抽象類別，實作常用的方法，由於每一個 repository 對應的 model 都不同，因此我們保留 model() 這個方法到每一個 repository 再實作
```php
namespace App\Repositories;

use Illuminate\Container\Container as App;

abstract class BaseRepository implements IRepository
{
    private $app;
    private $modelClass;

    public function __construct(App $app)
    {
        $this->app = $app;
        $this->modelClass = $this->model();
    }

    // 回傳各別 repository 要用的 model
    protected abstract function model();

    public function create(array $data)
    {
        $newModelInstance = $this->app->make($this->$modelClass);
        return $this->setModelInstance($newModelInstance, $data);
    }

    public function update($id, array $data)
    {
        $modelInstance = $this->$modelClass::find($id);
        return $this->setModelInstance($modelInstance, $data);
    }

    protected function setModelInstance($instance, array $data = [])
    {
        if (isset($instance)) {
            foreach($data as $property => $value) {
                if (isset($value)) {
                    $instance[$property] = $value;
                }
            }

            $saveSuccess = $instance->save();
            if ($saveSuccess) {
                return $instance;
            }
        }
        throw new \Exception("create/update failed");
    }

    public function delete($id)
    {
        $this->modelClass::destroy($id);
    }

    public function readById($id, $collumns = ['*'])
    {
        return $this->modelClass::find($id, $collumns);
    }

    // ...
}
```
03. 改寫 PostRepository
```php
class PostRepository extends BaseRepository
{
    protected function model() {
        return Post::class;
    }
}
```
---
透過實作 IRepository 與 繼承 BaseRepository，之後所有我們要用到的 repositories 更可以專注在自己特例的功能上，維護與撰寫測試會更加方便與彈性。順帶一提，範例中各種 Eloquent Model 的功能可以參考[官方 API](https://laravel.com/api/5.8/Illuminate/Database/Eloquent/Model.html)。

---
Repository 主要的重點在於為 controller 瘦身，把與資料表存取的邏輯集中在一起，因此假若未來更換資料庫，例如從 MySql 改為 MongoDB 我們只要更新各個 repositories 即可，不影響到原本的商業邏輯!

明天我們將繼續介紹相對易懂的 service，把商業邏輯補齊吧!
