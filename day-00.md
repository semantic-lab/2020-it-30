## 前言
在 MVC 的開發階段的過程，在處理 view 的部分，由於多數框架渲染引擎的耦合性特別高，個人一直沒有好感~~(就像阿兩對小町)~~。

自從被前輩推坑進入 MVVM 的世界之後就回不去了，前後端分離不但可以讓兩頭同時開發，且前端畫面的常用元件，
終於可以站在巨人的肩膀上，不用自己造輪子啦! 節省許多開發與維護的時間呢!

隨著前後端分離的 SPA 因為 SEO(相對不優)、初次載入耗時兩大問題，逐漸再次轉向 SSR，只是仍然保持前後端分離的不變原則，所以 Nuxt.js、Next.js 也相繼出現。

在這次的踩坑分享中，主要紀錄、介紹透過 Laravel 與 Nuxt.js 兩大入門學習曲線較緩的框架，順利建立前後端分離的 SSR 網站!


## 採坑內容
此次紀錄踩坑內容大致分為以下重點:
1. 後端 Laravel
    * 專案結構介紹與相關設定
    * Migration
    * Model & Resource
    * Injection 依賴注入
    * Repository
    * Service
    * FormRequest
    * Validation
    * Controller
    * Middleware
    * Route
2. 番外篇: GIT sub-module
3. 前端 Vue & Nuxt
    * 專案結構介紹與相關設定
    * Vue Component 基礎屬性
    * Vue v-for
    * Vue emit 與自己的 v-model
    * Nuxt Page 與對應的 Route
    * Nuxt Vuex & Cookie
    * API 串接與 validation
    * Nuxt Plugin
    * Nuxt no-ssr
    * 造個輪子玩玩: login & logout
    * 造個輪子玩玩: Form
    * 造個輪子玩玩: Table
4. Scss 略懂略懂
5. docker 部屬
    * Laravel
    * Nuxt
    
    
    



