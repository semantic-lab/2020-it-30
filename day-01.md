# 開發環境 & 套件
今天主要先把開發環境準備好，並建立專案。

喔對了! 才剛開始鐵人馬上被打臉，本次的採坑過程中主要是以 Laravel 5.8 為主，
但就在今天!! Laravel 推出 6.0 版本啦 (解釋解釋，甚麼叫驚喜 XD)! 看了 release notes，
還好還支援 5.8，不然臉也太腫了! XD

* [Laravel](#laravel)
* [Nuxt.js](#nuxt.js)
* 參考資料

## Laravel
01. 下載、安裝 [WampServer](http://www.wampserver.com/en/) 整合包，方便開發
    * 安裝過程若缺少"msvcp120.dll"，可以下載[Visual Studio 2013 Visual C++](https://www.microsoft.com/zh-TW/download/details.aspx?id=40784) 解決
    * (可替換為其他整合包)
02. 環境變數 "path" 加入要使用的 php 版本路徑，例如: \{wamp_安裝的資料夾}\wamp\bin\php\php7.3.5
03. 下載、安裝 [Composer](https://getcomposer.org/download/) 做為日後 php 套件管理的工具
    * 安裝過程記得要選擇 "步驟 02" 路徑底下的 php.exe，其餘並不需要特別勾選選項，使用預設就可以
    * 重開機，開啟 cmd 執行下面指令，如果有出現版本號，就代表安裝成功啦!
    ```shell script
    composer -V
    ```
04. 開啟 cmd 執行下面指令，安裝全局的 Laravel 套件:
    ```shell script
    composer global require laravel/installer
    ```
    * 輸入下面指令，確認安裝成功
    ```shell script
    laravel -V
    ```
05. 開啟 cmd 切換到詹案要存放的路徑，併入下面指令建立新 Laravel 專案
    ```shell script
    laravel new <專案名稱>
    ```
06. 最後，我們要設定專案與 apache 之間的路徑對應，之後我們不用每次都要把專案存在特定地方，而且在本機還可以有自己的 domain (甚麼都 127.0.0.1 或是 localhost 多沒創意 XD)
    01. 開啟 `{wamp 安裝路徑}\bin\apache\apache2.4.33\conf\extra\httpd-vhosts.conf`，輸入下列資訊， 其中，`DocumentRoot`, `ServerName`, `ServerAlias` 和 `Directory` 需要設定
    ```
    <VirtualHost *:80>
        DocumentRoot {Laravel 專案路徑}\public
        ServerName {自訂 URI}
        ServerAlias {自訂 URI}
        <Directory  "{Laravel 專案路徑}\public">
            Options +Indexes +Includes +FollowSymLinks +MultiViews
            AllowOverride All
            Require local
        </Directory>
    </VirtualHost>
    ```
    02. 開啟 `C:\Windows\System32\drivers\etc\hosts` 輸入下列資訊，其中 `{自訂的 URI}` 要跟httpd-vhosts.conf 設定的一樣
    ```
    127.0.0.1 {自訂的 URI}
    ```
    03. 重啟 wamp 

## Nuxt.js
01. 下載、安裝 [Node.js](https://nodejs.org/en/download/)
    * 輸入下面指令，確認 Node.js 與 npm (JavaScript 套件管理工具) 安裝成功
    ```shell script
    node -v
    ```
    ```shell script
    npm -v
    ```
02. 輸入下面指令安裝 Nuxt.js 
    ```shell script
    npx create-nuxt-app <專案名稱>
    ```
    * 若安裝過程遇到: **"Could not install from "..." as it does not contain a package.json file"**
        01. 檢查 "C:\Users\<USER_NAME>\AppData\Roaming\npm-cache"
        02. 若 <USER_NAME> 是包含空格，請將空格置換為 "~1"
            * First Name --> First~1Name
        03. 輸入下列指令，設定 cache
        ```shell script
        npm config set cache "C:\Users\<USER_NAME>\AppData\Roaming\npm-cache"
        ```
        04. 重新輸入 Nuxt.js 安裝指令



開發環境設定好、也建立專案了，明天終於可以寫 Laravel 了!


## 參考資料
* [Laravel 官網](https://laravel.com/docs/5.8)
* [Nuxt.js 官網](https://nuxtjs.org/guide/installation#using-nuxt-js-starter-template)
